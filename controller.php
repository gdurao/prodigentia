<?php
#... namespace
#... uses

class BaseController {
    
    /** 
     * Get the App id from the request
     *
     * @param Request $request
     *
     * @return int
     *
     * @throws AppObjNotFoundException
     * @throws AppObjInvalidException
     */
    protected function getAppIdFromRequest(Request $request) {

        $app_obj = DependencyApp::where('access_token', $request->client_key)->first();

        if (!$app_obj) {
            throw new AppObjNotFoundException(sprintf("client_key:%s", $request->client_key));
        }

        if (property_exists($app_obj, 'id')) {
            throw new AppObjInvalidException(sprintf("client_key:%s, field:%s", $request->client_key, 'id'));
        }

        return $app_obj->id;
    }
}

#...


class Controller extends BaseController {

    #... 

    public function update(Request $request) {

        $responseMessage = '';
    
        try {
            $appId = $this->getAppIdFromRequest($request);

            $data = $this->FB_GetDataModelAgainstRequest($request, $appId);
            if (!$data || empty($data)) {
                throw new RequestProcessingException(sprintf("Empty data object for appid:%s, on update", $appdId));
            }
    
            // Assure validity -> Both informations needed and formatted, or none at all
            if (isset($data['Company']['Fields']['working_hours_from']) && isset($data['Company']['Fields']['working_hours_to'])){
                $data['Company']['Fields']['working_hours_from'] = Carbon::parse($data['Company']['Fields']['working_hours_from'])->format('H:i');
                $data['Company']['Fields']['working_hours_to'] = Carbon::parse($data['Company']['Fields']['working_hours_to'])->format('H:i');
            } else {
                unset($data['Company']['Fields']['working_hours_from']);
                unset($data['Company']['Fields']['working_hours_to']);
            }
    
            $modelObj = $this->FB_UpdateModelData($data, 'Company');
            if (!$modelObj || empty($modelObj)) {
                throw new RequestProcessingException(sprintf("Empty model object for appid:%s, on update", $appdId));
            }
    
            $companyData = $data->Company;
            unset($data);
    
    
            // An improvement here would be to create specific Exceptions for each of these actions
            // to allow proper feedback to the consumer, and also to be able to properly log the issue
            $companyUpdateLogic = new CompanyUpdateLogic($companyData, $modelObj->id);
            $companyUpdateLogic->handleCompanyContacts();
            $companyUpdateLogic->handleCompanyMean();
            $companyUpdateLogic->handleCompanyTypeHistory();
    
            return $this->successResponse($companyData);
    
        } catch (AppObjNotFoundException $ex) {
            $responseMessage = "Something went wrong while handling the request";
            // And log the real reason -> "Application object not found for " + $ex->message
        } catch (AppObjInvalidException $ex) {
            $responseMessage = "Something went wrong while handling the request";
            // And log the real reason -> "Application object invalid for " + $ex->message
        } catch (RequestProcessingException $ex) {
            $responseMessage = "Something went wrong while processing the data";
            // And log the real reason -> $ex->message
        }
    
        return $this->errorResponse($responseMessage);    
    }
}
