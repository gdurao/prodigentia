<?php
#... namespace
#... uses


class CompanyUpdateLogic {

    /**
     * Company data
     * 
     * @var array<string, mixed> 
     */
    protected $companyData;

    /**
     * Company id
     * 
     * @var int 
     */
    protected $companyId;

    /**
     * Logic for Company Update/Create.
     *
     * @param array $companyData The company info
     * @param int   $companyId   The company id
     */
    public function __construct($companyData, $companyId)
    {
        $this->companyData = $companyData;
        $this->companyId   = $companyId;
    }

    /**
     * Handle Create/Update/Delete contacts
     * Possible tests: Against different provider data, assure that contacts are correctly choosen to be
     * create, updated, or deleted
     */
    public function handleCompanyContacts() {

        $companyContactToMaintain = [];

        if (!empty($this->companyData['company_contacts'])) {

            foreach($this->companyData['company_contacts'] as $companyContact) {

                $updateKey = [];

                if (isset($companyContact['id'])) {
                    $updateKey = ['id' => $companyContact['id']];
                    unset($companyContact['id']);
                }

                $companyContact['company_id'] = $companyId;

                $result                     = CompanyContact::updateOrCreate($updateKey, $companyContact);
                $companyContactToMaintain[] = $result->id;
            }
        }

        // Reversing the logic. Maintain the contacts that exist in $companyData['company_contacts'],
        // Remove the rest (within the same company, of course) 
        // Saves us the initial GET for current records
        if (count($companyContactToMaintain) != count($this->companyData['company_contacts'])) {
            CompanyContact::where('company_id', $this->companyId)
                        ->whereNotIn('id', $companyContactToMaintain)->delete();
        }
    }

    /**
     * Handle Create/Update/Delete Means
     * Possible tests: Against different provider data, assure that the company means are correctly choosen to be
     * create, updated, or deleted
     */
    public function handleCompanyMean() {
        
        $companyLoadingMeanToMaintain = [];

        if (isset($this->companyData['has_loading_means']) && $this->companyData['has_loading_means'] > 0) {
            foreach($this->companyData['loading_means'] as $loadingMean) {

                $tempData                    = [];
                $tempData['loading_mean_id'] = $loadingMean;
                $tempData['company_id']      = $modelObj->id;

                $result                         = CompanyLoadingMean::updateOrCreate($tempData, $tempData);
                $companyLoadingMeanToMaintain[] = $result->loading_mean_id;
            }
        }

        // Reversing the logic. Maintain the contacts that exist in $companyData['loading_means'],
        // Remove the rest (within the same company, of course) 
        // Saves us the initial GET for current records
        if (count($companyLoadingMeanToMaintain) != count($this->companyData['loading_means'])) {
            CompanyLoadingMean::where('company_id', $this->companyId)
                        ->whereNotIn('id', $companyLoadingMeanToMaintain)->delete();
        }
    }

    /**
     * Handle Create/Update/Delete Means
     * Possible tests: Against different provider data, assure that the company type history items are correctly choosen to be
     * create, updated, or deleted
     */
    public function handleCompanyTypeHistory() {

        $companyTypeHistoryToMaintain = [];

        if (isset($this->companyData['company_type'])) {
            if (!is_array($this->companyData['company_type']) ) {
                $this->companyData['company_type'] = [$this->companyData['company_type']];
            }

            foreach($this->companyData['company_type'] as $companyType) {
                $tempData                    = [];
                $tempData['company_type_id'] = $companyType;
                $tempData['company_id']      = $model_obj->id;

                $result                         = CompanyTypeHistory::updateOrCreate($tempData, $tempData);
                $companyTypeHistoryToMaintain[] = $result->company_type_id;
            }

        }

        // Reversing the logic. Maintain the contacts that exist in $companyData['company_type'],
        // Remove the rest (within the same company, of course) 
        // Saves us the initial GET for current records
        if (count($companyTypeHistoryToMaintain) != count($this->companyData['company_type'])) {
            CompanyTypeHistory::where('company_id', $this->companyId)
                        ->whereNotIn('id', $companyTypeHistoryToMaintain)->delete();
        }
    }
}
