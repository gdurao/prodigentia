 <?php
 
 public function update(Request $request) {
        $app_obj = DependencyApp::where('access_token', $request->client_key)->first();

        // Unnecessary, has no further action in the code. Removed from solution
        $processed_default_variables = array_merge($request->input('Company'), [
            'app_id' => $app_obj->id,
        ]);

        $data = $this->FB_GetDataModelAgainstRequest($request, $app_obj->id);
        if(!$data || empty($data)) return response()->json(['error' => 'Something went wrong while processing the data.'], 200);

        // Unnecessary, just re-doing what FB_GetDataModelAgainstRequest is supposed to have made. Removed from solution
        if( !isset($data['Company']['Fields']['has_loading_means']) ) {
            $data['Company']['Fields']['has_loading_means'] = $request->has_loading_means;
        }

        // This validation does not account the case where only one of the values exist
        if(isset($data['Company']['Fields']['working_hours_from']) && isset($data['Company']['Fields']['working_hours_to'])){
            $data['Company']['Fields']['working_hours_from'] = Carbon::parse($data['Company']['Fields']['working_hours_from'])->format('H:i');
            $data['Company']['Fields']['working_hours_to'] = Carbon::parse($data['Company']['Fields']['working_hours_to'])->format('H:i');
        }

        $model_obj = $this->FB_UpdateModelData($data, 'Company');
        // Testing the wrong resource
        if(!$data || empty($data)) return response()->json(['error' => 'Something went wrong while saving the data.'], 200);

        // I assumed that this info is already on $data
        $company_data = $request->input('Company');
        // Might as well just unset $data, as it is not needed anymore
        unset($data['client_key']);


        $companyContactToDelete = [];

        if (isset($company_data['company_contacts']) && !empty($company_data['company_contacts'])) {

            foreach($company_data['company_contacts'] as $companyContact) {

                $updateKey = [];

                if (isset($companyContact['id'])) {

                    $updateKey  = [ 'id' => $companyContact['id'] ];
                    unset($companyContact['id']);

                    // This IF will never be true
                    if (empty($companyContact)) {
                        $companyContactToDelete[] = $updateKey['id'];
                    }
                }

                $companyContact['company_id'] = $model_obj->id;

                CompanyContact::updateOrCreate($updateKey, $companyContact);
            }
        }

        // This will always be false because above the IF was never true
        // So, $companyContactToDelete is always empty
        if (count($companyContactToDelete) > 0) {
            CompanyContact::whereIn('id', $companyContactToDelete)->delete();
        }


        // Using this approach we will always waste a Query
        $companyLoadingMeanToDelete = CompanyLoadingMean::where('company_id', $model_obj->id)->pluck('loading_mean_id', 'loading_mean_id');

        if (isset($company_data['has_loading_means'])) {
            if ($company_data['has_loading_means'] != 0) {
                foreach($company_data['loading_means'] as $loadingMean) {

                    $data = [];
                    $data['loading_mean_id']    = $loadingMean;
                    $data['company_id']         = $model_obj->id;

                    unset($companyLoadingMeanToDelete[$loadingMean]);

                    CompanyLoadingMean::updateOrCreate($data, $data);
                }
            }
        }

        if (count($companyLoadingMeanToDelete) > 0) {
            CompanyLoadingMean::where('company_id', $model_obj->id)->whereIn('loading_mean_id', $companyLoadingMeanToDelete)->delete();
        }

        // Using this approach we will always waste a Query
        $companyTypeHistoryToDelete = CompanyTypeHistory::where('company_id', $model_obj->id)->pluck('company_type_id', 'company_type_id');

        if (isset($company_data['company_type'])) {
            if (!is_array($company_data['company_type']) ) {
                $company_data['company_type'] = [ $company_data['company_type'] ];
            }

            foreach($company_data['company_type'] as $companyType) {
                $data = [];
                $data['company_type_id']    = $companyType;
                $data['company_id']         = $model_obj->id;

                unset($companyTypeHistoryToDelete[$companyType]);

                CompanyTypeHistory::updateOrCreate($data, $data);
            }

        }

        if (count($companyTypeHistoryToDelete) > 0) {
            CompanyTypeHistory::where('company_id', $model_obj->id)->whereIn('company_type_id', $companyTypeHistoryToDelete)->delete();
        }

        return $this->successResponse($model_obj);
    }
